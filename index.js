var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var d3 = require('d3');
const Tail = require('tail').Tail;
var tail = new Tail("database.txt");
tail.watch();
tail.on("line", data => {
        io.emit('data received', data);
        console.log(data);
    });

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/d3/d3.min.js', function(req, res){
  res.sendFile(__dirname + '/node_modules/d3/d3.min.js');
});

io.on('connection', function(socket){
    console.log("Connection established");
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
